package customviews;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import com.rajshree.service_new.R;
import com.rajshree.service_new.interfacess.SelectionInterface;


public class BottomSheetSelectionFragment extends BottomSheetDialogFragment {
    public static SelectionInterface mListener;

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        //Set the custom view
        View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_select_deselect, null);
        final CustomTextView bs_select_all = view.findViewById(R.id.bs_select_all);
        final CustomTextView bs_de_select_all = view.findViewById(R.id.bs_de_select_all);

        dialog.setContentView(view);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        final CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    String state = "";

                    switch (newState) {
                        case BottomSheetBehavior.STATE_DRAGGING: {
                            state = "DRAGGING";
                            break;
                        }
                        case BottomSheetBehavior.STATE_SETTLING: {
                            state = "SETTLING";
                            break;
                        }
                        case BottomSheetBehavior.STATE_EXPANDED: {
                            state = "EXPANDED";
                            break;
                        }
                        case BottomSheetBehavior.STATE_COLLAPSED: {
                            state = "COLLAPSED";
                            break;
                        }
                        case BottomSheetBehavior.STATE_HIDDEN: {
                            dismiss();
                            state = "HIDDEN";
                            break;
                        }
                    }

                    //Toast.makeText(getContext(), "Bottom Sheet State Changed to: " + state, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            });
        }

        bs_de_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((BottomSheetBehavior) behavior).getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    dismiss();
                    if (mListener != null) {
                        mListener.refreshSelection(bs_de_select_all.getText().toString());
                    }
                }
            }
        });

        bs_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((BottomSheetBehavior) behavior).getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    dismiss();
                    if (mListener != null) {
                        mListener.refreshSelection(bs_select_all.getText().toString());
                    }
                }
            }
        });
    }


    public static void bindListener(SelectionInterface listener) {

        mListener = listener;
    }
 /*   void showBootmSheet() {
        if (sheetBehavior ?.getState() != BottomSheetBehavior.STATE_EXPANDED){
            sheetBehavior ?.setState(BottomSheetBehavior.STATE_EXPANDED);
            //btnBottomSheet.setText("Close sheet");
            layoutBottomSheet !!.visibility = View.GONE
            Log.d("sdsds", "ooo")
        } else{
            sheetBehavior ?.setState(BottomSheetBehavior.STATE_COLLAPSED);
            //btnBottomSheet.setText("Expand sheet");
            layoutBottomSheet !!.visibility = View.VISIBLE
            Log.d("sdsds", "1111")
        }
    }*/
}
