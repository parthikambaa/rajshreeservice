package com.rajshree.service_new.interfacess

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.util.HashMap

interface RetrofitService {

  @POST("{path}")
  @FormUrlEncoded
  fun call_post_get(@Path("path") path: String, @Header("Authorization") auth: String, @FieldMap map: HashMap<String, Any>): Call<ResponseBody>
  @POST("{path}")
  fun call_post(@Path("path") path: String, @Body map: HashMap<String, Any> ):Call<ResponseBody>
  @POST("{path}")
  fun call_post(@Path("path") path: String, @Header("Authorization") auth: String, @Body map: HashMap<String, Any>): Call<ResponseBody>

  @Multipart
  @POST("{path}")
   fun call_post(
    @Path("path") method: String,
    @Header("Authorization") auth: String,
    @Part("chassis_no") chassisno: RequestBody,
    @PartMap map: HashMap<String, Any>,
    @Part("complaint") complaints: RequestBody,
    @Part("enquiry_id") enquiry_id: RequestBody,
    @Part("userid") userid: RequestBody,
    @Part complaintImage: MultipartBody.Part
  ): Call<ResponseBody>




}