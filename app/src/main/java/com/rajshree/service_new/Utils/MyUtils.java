package com.rajshree.service_new.Utils;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import com.rajshree.service_new.R;
import com.rajshree.service_new.apiandlocalsession.RetrofitCall;
import com.rajshree.service_new.interfacess.RetrofitService;
import com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo.EnquiriesItem;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;


@SuppressWarnings("All")
public class MyUtils {


    public static boolean isSelect = false;
    public static String CAL_MONTH = "";
    public static String CAL_YEAR = "";

    public static String LEAVE_CAL_DATE = "";
    public static String LEAVE_CAL_MONTH = "";
    public static String LEAVE_CAL_YEAR = "";


    public static String PRIVACY_POLICY = "https://policies.google.com/privacy?hl=en-US";


    public  static String  SALES ="Sales";
    public  static String  SERVICE ="Service";
    public  static String  MAHINDRA ="Mahindra & Mahindra";
    @Nullable
    public static final String SELECT_CITY="Select City";


    /**
     * Pass fragment without back stack
     *
     * @param fragmentActivity
     * @param fragment
     */
    public static void passFragmentWithoutBackStack(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commitAllowingStateLoss();

    }

    public static void passFragmentWithoutBackStackanima(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, fragment)
                //.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,R.anim.slide_in_right,R.anim.right_to_left)
                //.setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
                .show(fragment)
                .commitAllowingStateLoss();

    }

    public static void bottomtoTop(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, fragment)
                //.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up,R.anim.slide_out_up,R.anim.slide_in_up)
                //.setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
                .show(fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();

    }

    public static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    /**
     * Pass Fragment with Back Stack Mode
     *
     * @param fragmentActivity
     * @param fragment
     */
    public static void passFragmentBackStack(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).addToBackStack(null).commitAllowingStateLoss();

    }

    public static void passFragmentBackStackwithani(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity
                .getSupportFragmentManager()
                .beginTransaction()
                //.setCustomAnimations(R.anim.slide_in_right,R.anim.right_to_left,R.anim.slide_in_left, R.anim.slide_out_right)
                .show(fragment)
                //.setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
                .replace(R.id.main_container, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();

    }

    /**
     * Check Build Version
     *
     * @return
     */
    public static int checkVersion() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            //view.setBackgroundResource(R.drawable.ripple_center);
            return 1;
        } else {
            return 0;
        }
    }


    /**
     * Init Retrofit
     *
     * @return
     */

    /**
     * Check Two Digit number
     *
     * @param inputDigits
     * @return
     */
    public static String checkTwoDigitNumber(int inputDigits) {
        if (inputDigits <= 9) {
            return "0" + inputDigits;
        } else {
            return String.valueOf(inputDigits);
        }
    }

    /**
     * Sorting Decending
     *
     * @param sortingValues
     * @return
     */

    public static ArrayList<HashMap<String, String>> sortingZ_A(ArrayList<HashMap<String, String>> sortingValues) {
        Collections.sort(sortingValues, new Comparator<HashMap<String, String>>() {
            @Override
            public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
                return o2.get("dates").compareTo(o1.get("dates"));
            }
        });

        return sortingValues;
    }


    public static void darkenStatusBar(Activity activity, int color) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            activity.getWindow().setStatusBarColor(darkenColor(ContextCompat.getColor(activity, color)));
        }

    }


    // Code to darken the serviceType supplied (mostly serviceType of toolbar)
    private static int darkenColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f;
        return Color.HSVToColor(hsv);
    }


    /**
     * Check Empty Values
     */

    public static boolean isEmpty(FragmentActivity appCompatActivity, String input, String errorMessage) {
        if (input.isEmpty()) {
            MessageUtils.showToastMessage(appCompatActivity, errorMessage);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Check Empty Values
     */

    public static boolean isEmptySnackView(FragmentActivity appCompatActivity, View view, String input, String errorMessage) {
        if (input.isEmpty()) {
            MessageUtils.showSnackBar(appCompatActivity, view, errorMessage);
            return true;
        } else {
            return false;
        }
    }


    public static Typeface getTypeface(FragmentActivity activity, String font) {
        return Typeface.createFromAsset(activity.getAssets(), font);
    }


    /**
     * ConvertInputStream to String
     */

    public static String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        return sb.toString();
    }


    @SuppressLint("ResourceAsColor")
    public static void focusable(EditText editText, boolean status) {
        editText.setFocusableInTouchMode(status);
        editText.setLongClickable(status);
        editText.setFocusable(status);
        if (!status) {
            editText.setBackgroundColor(android.R.color.transparent);
        } else {
            editText.setBackgroundColor(android.R.color.black);
        }
    }

    public static String nullPointerValidation(@Nullable String inputStr) {
        try {

            //Log.d("Whatss", "" + inputStr);



            /*if (inputStr != null) {
                Log.d("Whatss", "0000sss");
            } else if (inputStr == null) {
                Log.d("Whatss", "0011ssss");
            }

            if (inputStr.equals(null)) {
                Log.d("Whatss", "1111");
            } else if (!inputStr.equals(null)) {
                Log.d("Whatss", "1100");
            }
*/

            if (inputStr == null) {
                return "-";
            } else if (inputStr.isEmpty()) {
                return "-";
            } else {
                return inputStr;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return inputStr;
        }

    }


    public static Typeface setType(FragmentActivity activity, String fontType) {
        return Typeface.createFromAsset(activity.getAssets(), String.valueOf(fontType));
    }

    public static RetrofitService getRetroService() {
        return RetrofitCall.Companion.getClient().create(RetrofitService.class);
    }

    public static boolean isNetworkConnected(FragmentActivity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    public static void PrivacyPolicy(FragmentActivity activity, String Url) {
        try {

            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            builder.addDefaultShareMenuItem();
            builder.setExitAnimations(activity, R.anim.slide_in_right, R.anim.right_to_left);
            builder.setStartAnimations(activity, R.anim.slide_in_left, R.anim.slide_out_right);
            builder.setSecondaryToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(activity, Uri.parse(Url));
        } catch (Exception x) {
            x.printStackTrace();
        }
    }


    public static ArrayList<EnquiriesItem> sort(ArrayList<EnquiriesItem> temp){

        Collections.sort(temp, new Comparator<EnquiriesItem>() {
            @Override
            public int compare(EnquiriesItem o1, EnquiriesItem o2) {
                return o2.getEnquiry_date().compareTo(o1.getEnquiry_date());
            }
        });
        return temp;
    }

    @NotNull
    public static String nullValidation( String varible) {
       if(varible!=null){
           if(varible.isEmpty()|| varible.equals("null") ){
               return "-";
           }else{
               return  varible.trim();
           }
       } else{
           return  "-";
       }
    }
}
