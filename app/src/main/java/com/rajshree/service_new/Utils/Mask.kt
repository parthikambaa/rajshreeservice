package com.rajshree.service_new.Utils

import android.text.Editable
import android.widget.EditText
import android.text.TextWatcher
import android.util.Log
import com.rajshree.service_new.R
import kotlinx.android.synthetic.main.app_bar_home.view.*
import java.util.*


class DateInputMask(private val input: EditText) : TextWatcher {

    private var current = ""
    private val ddmmyyyy = ""
    private val cal = Calendar.getInstance()

    init {
        this.input.addTextChangedListener(this)
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (s.toString() == current) {
            return
        }

        var clean = s.toString().replace("[^\\d.]|\\.".toRegex(), "")

        val cleanC = current.replace("[^\\d.]|\\.".toRegex(), "")
        Log.d("Watcher"," Clean and c$clean  $cleanC")

        val cl = clean.length
        Log.d("Watcher", " cl $cl")
        var sel = cl
        Log.d("Watcher","sel  $sel")
        var i = 2
        while (i <= cl && i < 6) {
            Log.d("Watcher"," i$i   $sel ")
            sel++
            i += 2
        }
        Log.d("Watcher"," i$i   $sel ")
        //Fix for pressing delete next to a forward slash
        if (clean == cleanC) sel--
        Log.d("Watcher","sel   $sel ")

        if (clean.length < 8) {
//            clean += ddmmyyyy.substring(clean.length)
            Log.d("Watcher","clean   $clean ")
        } else {
            //This part makes sure that when we finish entering numbers
            //the date is correct, fixing it otherwise
            var day = Integer.parseInt(clean.substring(0, 2))
            Log.d("Watcher","day   $day ")
            var mon = Integer.parseInt(clean.substring(2, 4))
            Log.d("Watcher","mon   $day ")
            var year = Integer.parseInt(clean.substring(4, 8))
            Log.d("Watcher","year   $day ")

            mon = if (mon < 1) 1 else if (mon > 12) 12 else mon
            cal.set(Calendar.MONTH, mon - 1)
            year = if (year < 1900) 1900 else if (year > 2100) 2100 else year
            cal.set(Calendar.YEAR, year)
            // ^ first set year for the line below to work correctly
            //with leap years - otherwise, date e.g. 29/02/2012
            //would be automatically corrected to 28/02/2012

            day = if (day > cal.getActualMaximum(Calendar.DATE)) cal.getActualMaximum(Calendar.DATE) else day
            clean = String.format("%02d%02d%02d", day, mon, year)
            Log.d("Watcher","clean   $clean ")
            clean = String.format( "%s-%s-%s", clean.substring(0, 2), clean.substring(2, 4), clean.substring(4, 8)   )
        }


        Log.d("Watcher","clean   $clean ")

        sel = if (sel < 0) 0 else sel
        Log.d("Watcher","sel   $sel ")
        current = clean
        input.setText(current)
        input.setSelection(if (s.length < current.length) sel else current.length)
        Log.d("Watcher","sel   $current ")

    }

    override fun afterTextChanged(s: Editable) {
               Log.d("WatcherAFTER","${s.toString().length}")
        if(s.isNotEmpty()){
            if(s.toString().length==10){
               if(checkExpiryDate(s.toString())){
                   input.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_approved,0)
               }else{
                   input.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_reject,0)
               }
            }
        }
    }

    private fun checkExpiryDate(date: String): Boolean {
        try {
            val currentDateTimemilli  = Calendar.getInstance().timeInMillis
            val  textedDatetimeMilli = DateUtils.geTimeMillisecond(date)

            val currentYear = DateUtils.Year(currentDateTimemilli)
            val currentMonth = DateUtils.Month(currentDateTimemilli)
            val currentDate = DateUtils.date(currentDateTimemilli)

            Log.d("CurrentDate","$currentYear  $currentMonth $currentDate")


            val  textedYear = DateUtils.Year(textedDatetimeMilli)
            val  textedMonth = DateUtils.Month(textedDatetimeMilli)
            val  textedDate = DateUtils.date(textedDatetimeMilli)

            Log.d("Texted","$textedYear $textedMonth $textedDate ")

            return if(currentYear.toInt()-1 <= textedYear.toInt()  && textedYear.toInt()<= currentYear.toInt()+3 ){

                if((currentYear.toInt()-1) ==textedYear.toInt()){
                    Log.d("AFTER","$currentMonth $textedMonth  $currentDate $textedDate ")
                    currentMonth.toInt()<=textedMonth.toInt() &&  currentDate.toInt()<= textedDate.toInt()

                }else{
                    true
                }
            }else{
                false
            }

        }catch (ex:Exception){
            ex.toString()
        }
        return true
    }
}