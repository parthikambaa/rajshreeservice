package com.rajshree.service_new.Utils;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.rajshree.service_new.R;

import java.util.Objects;


public class MessageUtils {

    /**
     * Show messge using snack Bar
     *
     * @param context
     * @param view
     * @param message
     */
    public static Snackbar snackbar;

    @SuppressLint("NewApi")
    public static Snackbar showSnackBar(Context context, View view, String message) {
        try {

            snackbar = Snackbar.make(view, String.valueOf(message), Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
            TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setGravity(Gravity.CENTER);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textView.setTextSize(16);
       /* Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "Typomoderno_bold.ttf");
        textView.setTypeface(custom_font);*/
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            return snackbar;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Show ERROR/SUCCESS  messge using toast SHORT time
     *
     * @param context
     * @param msg
     */

    public static void showToastMessage(Context context, String msg) {
        Toast toast = Toast.makeText(context, String.valueOf(msg), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    /**
     * * Show ERROR/SUCCESS messge using toast LONG time
     *
     * @param context
     * @param msg
     */
    public static void showToastMessageLong(FragmentActivity context, String msg) {
        Toast toast = Toast.makeText(context, String.valueOf(msg), Toast.LENGTH_LONG);
        //toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    /**
     * Show Progress dialog
     *
     * @param activity
     * @return
     */

    public static Dialog showDialog(FragmentActivity activity) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle(null);
        ProgressBar progressBar = new ProgressBar(activity);
        progressBar.setIndeterminateDrawable(activity.getResources().getDrawable(R.drawable.my_progress_indeterminate));
        dialog.setContentView(progressBar);
        Objects.requireNonNull(dialog.getWindow()).setWindowAnimations(R.style.grow);
        dialog.getWindow().setBackgroundDrawableResource(R.color.dialg_trans);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;

    }

    public static Dialog showDialogWhite(FragmentActivity activity) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle(null);
        ProgressBar progressBar = new ProgressBar(activity);
        progressBar.setIndeterminateDrawable(activity.getResources().getDrawable(R.drawable.my_progress_indeterminates));
        dialog.setContentView(progressBar);
        Objects.requireNonNull(dialog.getWindow()).setWindowAnimations(R.style.grow);
        dialog.getWindow().setBackgroundDrawableResource(R.color.dialg_trans);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;

    }


    public static Dialog dismissDialog(Dialog dialog) {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return dialog;
    }

    /**
     * Show Network Connection Dialog
     *
     * @param activity
     * @return
     */
    public static Dialog showNetworkDialog(final FragmentActivity activity) {

        final Dialog netWorkDialog = new Dialog(activity);
        netWorkDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        netWorkDialog.setContentView(R.layout.dialog_internet);
        netWorkDialog.setCancelable(false);
        netWorkDialog.getWindow().setWindowAnimations(R.style.grow);
        TextView gotoSt = netWorkDialog.findViewById(R.id.gotoSt);
        TextView cancel = netWorkDialog.findViewById(R.id.cancel);
        gotoSt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                netWorkDialog.dismiss();
                try {
                    Intent intent = new Intent();
                    //$DataUsageSummaryActivity
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    activity.startActivity(intent);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
        netWorkDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                netWorkDialog.dismiss();
                activity.finish();
                MessageUtils.showToastMessageLong(activity, activity.getResources().getString(R.string.net_message));
                return false;
            }
        });
        netWorkDialog.show();
        return netWorkDialog;
    }

    /**
     * @param context
     * @param view
     * @param message
     * @return
     */


    @SuppressLint("NewApi")
    public static Snackbar showSnackBarAction(final FragmentActivity context, View view, String message) {

        Snackbar snackbar;
        snackbar = Snackbar.make(view, String.valueOf(message), Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        //textView.setGravity(Gravity.CENTER);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textView.setTextSize(16);
       /* Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "Typomoderno_bold.ttf");
        textView.setTypeface(custom_font);*/
        textView.setTextColor(Color.WHITE);
        snackbar.show();
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setAction("Go", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                context.startActivity(intent);
            }
        });
        return snackbar;


    }

    /**
     * Gto Settings from SnackView Action
     *
     * @param activity
     * @param view
     * @param message
     */

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static void snackActionSetting(final FragmentActivity activity, View view, String message) {
        Snackbar snackbar;
        snackbar = Snackbar.make(view, String.valueOf(message), Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        //textView.setGravity(Gravity.CENTER);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textView.setTextSize(16);
       /* Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "Typomoderno_bold.ttf");
        textView.setTypeface(custom_font);*/
        textView.setTextColor(Color.WHITE);
        snackbar.show();
        snackbar.setAction("Settings", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //    Intent intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                Intent intent = new Intent();
                //$DataUsageSummaryActivity
                intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                activity.startActivity(intent);
            }
        });

    }

/*
    private void blink(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 1000;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        TextView txt = (TextView) findViewById(R.id.usage);
                        if(txt.getVisibility() == View.VISIBLE){
                            txt.setVisibility(View.INVISIBLE);
                        }else{
                            txt.setVisibility(View.VISIBLE);
                        }
                        blink();
                    }
                });
            }
        }).start();
    }*/

    /**
     * Show Snack View
     *
     * @param activity
     * @param fontType
     * @return
     */
    public static Typeface setType(FragmentActivity activity, String fontType) {
        return Typeface.createFromAsset(activity.getAssets(), String.valueOf(fontType));
    }

    public static Snackbar setErrorMessage(FragmentActivity activity, View view, int response_code) {
        return showSnackBar(activity, view, showErrorCodeToast(response_code));
    }


    public static Snackbar setErrorKotlin(FragmentActivity activity, View view, int response_code) {
        return showSnackBar(activity, view, showErrorCodeToast(response_code));
    }

    /**
     * Checking Condition for Showing TSnack View
     *
     * @param response_code
     * @return
     */
    public static String showErrorCodeToast(int response_code) {
        Log.d("RESPONSE FOR ERROR CODE", "" + response_code);
        String msg = "";
        try {
            switch (response_code) {

                case 201:
                    msg = "200 -> Object created. Useful for the store actions";
                    //showSnackBar(activity, view, "Object created. Useful for the store actions");
                    break;
                case 204:
                    msg = "204 -> No content. When an action was executed successfully, but there is no content to return.";
                    //  showSnackBar(activity, view, "No content. When an action was executed successfully, but there is no content to return.");
                    break;
                case 206:
                    msg = "206 -> Partial content. Useful when you have to return a paginated list of resources.";
                    //showSnackBar(activity, view, "Partial content. Useful when you have to return a paginated list of resources.");
                    break;
                case 400:
                    msg = "400 -> Bad request. The standard option for requests that fail to pass validation";
                    // showSnackBar(activity, view, "Bad request. The standard option for requests that fail to pass validation");
                    break;
                case 401:
                    msg = "401 -> Bad request. The standard option for requests that fail to pass validation";
                    // showSnackBar(activity, view, "Bad request. The standard option for requests that fail to pass validation");
                    break;
                case 403:
                    msg = "403 -> Forbidden. The user is authenticated.";
                    //showSnackBar(activity, view, "Forbidden. The user is authenticated.");
                    break;
                case 404:
                    msg = "404 -> Server Not found";
                    //showSnackBar(activity, view, "SpareInvoicePojos not found");
                    break;
                case 405:
                    msg = "405 -> Method Not Allowed";
                    break;
                case 422:
                    msg = "422 -> unprocessed Request";
                    break;
                case 429:
                    msg = "429 -> Too Many Requests";
                    break;
                case 500:
                    msg = "500 -> Internal server error.";
                    // showSnackBar(activity, view, "Internal server error.");
                    break;
                case 503:
                    msg = "503 -> Service unavailable";
                    // showSnackBar(activity, view, "Service unavailable");
                    break;
                case 600:
                    msg = "600 -> Server not found";
                    // showSnackBar(activity, view, "SpareInvoicePojos not found");
                    break;
                default:
                    msg = "Server  not found";
                    //showSnackBar(activity, view, "SpareInvoicePojos not found");
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
        }
        return msg;
    }


    /**
     * Show Toast Message
     *
     * @param activity
     * @param view
     * @param response
     * @return
     */
    public static Snackbar setFailureMessage(FragmentActivity activity, View view, String response) {

        return showSnackBar(activity, view, showFailureToast(response));

    }


    public static Snackbar setFailureKotlin(FragmentActivity activity, View view, String response) {

        return showSnackBar(activity, view, showFailureToast(response));

    }

    /**
     * Check Condition for showing toast
     *
     * @param response
     * @return
     */
    public static String showFailureToast(String response) {
        Log.e("RESPONSE FOR ERROR", "" + response);
        String msg = "Something went worng";
        try {
            if (response.toLowerCase().contains("Unable to resolve host".toLowerCase())) {
                msg = "Please check your connection";
            } else if (response.toLowerCase().contains("Server not found".toLowerCase())) {
                msg = "Server not found";
            } else if (response.toLowerCase().contains("No route to host".toLowerCase())) {
                msg = "No route to host";
            } else if (response.toLowerCase().contains("Failed to connect to".toLowerCase())) {
                msg = "Failed to connect to Server";
            } else if (response.toLowerCase().contains("connect timed out".toLowerCase())) {
                msg = "Connection timed out";
            } else if (response.toLowerCase().equals("No Internet Connection".toLowerCase())) {
                msg = response;
            } else if (response.toLowerCase().contains("Expected BEGIN_ARRAY but was BEGIN_OBJECT at line ".toLowerCase())) {
                msg = "Formatted Please check API";
            } else if (response.toLowerCase().contains("End of input at line 1 column 1".toLowerCase())) {
                msg = "Formatted Please check API";
            } else {
                msg = "Please try again later.";
            }
        } catch (Exception e) {
            msg = e.getMessage();
        }

        return msg;

    }
}
