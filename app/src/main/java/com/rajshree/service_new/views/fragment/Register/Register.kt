package com.rajshree.service_new.views.fragment.Register


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.Gson
import com.rajshree.service_new.R
import com.rajshree.service_new.Utils.MessageUtils
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager
import com.rajshree.service_new.views.activity.CustomNavigation
import com.rajshree.service_new.views.fragment.Register.departmentpojo.DepartmentRES
import com.rajshree.service_new.views.fragment.Register.oraginationpojo.OrganizationListRES
import com.rajshree.service_new.views.fragment.Register.pojo.RegisterRES
import com.rajshree.service_new.views.fragment.login.Login
import kotlinx.android.synthetic.main.fragment_register.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern


class Register : Fragment() {

    var snackviewForREg: View? = null
    private val patternStr = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    var dialog: Dialog? = null
    var registerRes: RegisterRES? = null
    var sessionManager: LocalSessionManager? = null
    var organisationListRES: OrganizationListRES? = null
    var oraganizationmap: HashMap<String, Any>? = null
    var departmentmap: HashMap<String, Any>? = null
    var designationmap: HashMap<String, Any>? = null
    var departmentRES: DepartmentRES? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        (activity as CustomNavigation).enableViewTitle(getString(R.string.registeration))
        return inflater.inflate(R.layout.fragment_register, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        snackviewForREg = view.findViewById(R.id.snackview)
        sessionManager = LocalSessionManager()
        LocalSessionManager.LocalSessionManager(activity!!)


        when {
            MyUtils.isNetworkConnected(activity) -> {
                getorganization()
            }
            else -> {
                MessageUtils.showSnackBarAction(activity, snackviewForREg, getString(R.string.check_internet))
            }
        }


        /**
         * set oncheckedchangedListener
         */
        reg_organization_sp?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem = reg_organization_sp?.selectedItem.toString()
                reg_organization_edt.setText(selectedItem)
                if (selectedItem != getString(R.string.select_organization)) {

                    when {
                        MyUtils.isNetworkConnected(activity) -> {
                            getDepartmentDetails(selectedItem)
                        }
                        else -> {
                            MessageUtils.showSnackBarAction(
                                activity,
                                snackviewForREg,
                                getString(R.string.check_internet)
                            )
                        }
                    }


                }

            }

        }
        reg_depart_sp?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem = reg_depart_sp?.selectedItem.toString()
                reg_depart_edt.setText(selectedItem)

            }

        }
        reg_designation_sp?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem = reg_designation_sp?.selectedItem.toString()
                reg_designation_edt.setText(selectedItem)
            }

        }


        /***set onclick Listener**/
        reg_submit_btn?.setOnClickListener {
            try {
                val customername = reg_customer_name_edt?.text.toString()
                val mobile = reg_mob_no_edt?.text.toString()
                val email = reg_email_id_edt?.text.toString()
                val organisation = oraganizationmap!![reg_organization_edt?.text.toString()].toString()
                val department = departmentmap!![reg_depart_edt?.text.toString()].toString()
                val designation = designationmap!![reg_designation_edt?.text.toString()].toString()
                val password = reg_password_edt?.text.toString()
                if (validateforregister(customername, mobile, email, password, organisation, department, designation)) {
                    val map = HashMap<String, Any>()
                    map["customer_name"] = customername
                    map["mobile"] = mobile
                    map["password"] = password
                    map["email"] = email
                    map["organisation"] = organisation
                    map["department"] = department
                    map["designation"] = designation
                    if (MyUtils.isNetworkConnected(activity)) {
                        sendtoserverforREG(map)
                    } else {
                        MessageUtils.showSnackBarAction(activity, snackviewForREg, getString(R.string.check_internet))
                    }

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        reg_alreadyaccount_btn?.setOnClickListener {
            try {
                MyUtils.passFragmentWithoutBackStack(activity, Login())
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


    }

    private fun getDepartmentDetails(selectedItem: String) {
        try {
            dialog = MessageUtils.showDialogWhite(activity)
            val map = HashMap<String, Any>()
            map["organization_id"] = oraganizationmap!![selectedItem]!!
            val apiservice = MyUtils.getRetroService()
            val callback = apiservice.call_post("departmentlist", map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.showFailureToast(t.message)
                    MessageUtils.showSnackBar(activity, snackviewForREg, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            departmentRES = Gson().fromJson(response.body()?.string(), DepartmentRES::class.java)
                            if (departmentRES != null) {
                                if (departmentRES?.success!!) {
                                    if (departmentRES?.departments != null && departmentRES?.departments?.size != 0) {
                                        setTODeparmentADapater()
                                    } else {
                                        MessageUtils.showSnackBar(activity, snackviewForREg, departmentRES?.message)
                                    }
                                    if (departmentRES?.designation != null && departmentRES?.designation?.size != 0) {
                                        setTOdesignADapater()
                                    } else {
                                        MessageUtils.showSnackBar(activity, snackviewForREg, departmentRES?.message)
                                    }


                                } else {
                                    MessageUtils.showSnackBar(activity, snackviewForREg, departmentRES?.message)
                                }

                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    snackviewForREg,
                                    getString(R.string.check_json_format)
                                )
                            }

                        } else {
                            val msg = MessageUtils.showErrorCodeToast(response.code())
                            MessageUtils.showSnackBar(activity, snackviewForREg, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setTODeparmentADapater() {
        try {

            val itemlist = ArrayList<String>()
            departmentmap = HashMap()
            if (departmentRES?.departments?.size != 0) {
                for (item in departmentRES?.departments!!) {
                    itemlist.add(item?.department!!)
                    departmentmap!![item.department] = item.id!!
                }
                itemlist.add(0, getString(R.string.select_deparment))
            } else {
                itemlist.add(getString(R.string.data_not_available))
            }
            reg_depart_sp?.adapter =
                ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, itemlist)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setTOdesignADapater() {
        try {
            val itemlist = ArrayList<String>()
            designationmap = HashMap()
            if (departmentRES?.designation?.size != 0) {
                for (item in departmentRES?.designation!!) {
                    itemlist.add(item?.userType!!)
                    designationmap!![item.userType] = item.id!!
                }
                itemlist.add(0, getString(R.string.select_designation))
            } else {
                itemlist.add(getString(R.string.data_not_available))
            }
            reg_designation_sp?.adapter =
                ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, itemlist)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun getorganization() {
        try {
            dialog = MessageUtils.showDialogWhite(activity)
            val map = HashMap<String, Any>()
            val apiservice = MyUtils.getRetroService()
            val callback = apiservice.call_post("registerdata", map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.showFailureToast(t.message)
                    MessageUtils.showSnackBar(activity, snackviewForREg, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            organisationListRES =
                                Gson().fromJson(response.body()?.string(), OrganizationListRES::class.java)
                            if (organisationListRES != null) {
                                if (organisationListRES?.success!!) {
                                    if (organisationListRES?.organization != null && organisationListRES?.organization?.size != 0) {
                                        setTOADapater()
                                    } else {
                                        MessageUtils.showSnackBar(
                                            activity,
                                            snackviewForREg,
                                            organisationListRES?.message
                                        )
                                    }

                                } else {
                                    MessageUtils.showSnackBar(activity, snackviewForREg, organisationListRES?.message)
                                }

                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    snackviewForREg,
                                    getString(R.string.check_json_format)
                                )
                            }

                        } else {
                            val msg = MessageUtils.showErrorCodeToast(response.code())
                            MessageUtils.showSnackBar(activity, snackviewForREg, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setTOADapater() {
        try {
            val itemlist = ArrayList<String>()
            oraganizationmap = HashMap()
            if (organisationListRES?.organization?.size != 0) {
                for (item in organisationListRES?.organization!!) {
                    itemlist.add(item?.companyName!!)
                    oraganizationmap!![item.companyName] = item.id!!
                }
                itemlist.add(0, getString(R.string.select_organization))
            } else {
                itemlist.add(getString(R.string.data_not_available))
            }
            reg_organization_sp?.adapter =
                ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, itemlist)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun sendtoserverforREG(map: HashMap<String, Any>) {
        try {
            dialog = MessageUtils.showDialogWhite(activity)
            val calinterface = MyUtils.getRetroService()
//            val callback = calinterface.call_post("register", map)//Local server
            val callback = calinterface.call_post("appregister", map)//Live

            callback.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            /* registerRes = Gson().fromJson(response.body()?.string(), RegisterRES::class.java)
                             if (registerRes != null) {
                                 if (registerRes?.success!!) {
                                    *//* if (registerRes?.data != null) {
                                        Log.d("REGisterRES",""+ registerRes?.data?.userdetails?.mobile!!)
                                        LocalSessionManager.CreateSession(
                                            activity!!,
                                            registerRes?.data?.userdetails?.name!!,
                                            registerRes?.data?.userdetails?.mobile!!,
                                            registerRes?.data?.userdetails?.departmentId!!,
                                            registerRes?.data?.accessToken!!,
                                            registerRes?.data?.userdetails?.companyId!!,
                                            registerRes?.data?.userdetails?.email!!,
                                            registerRes?.data?.userdetails?.id!!
                                        )
                                    } else {
                                        MessageUtils.showSnackBar(
                                            activity,
                                            snackviewForREg,
                                            registerRes?.message
                                        )
                                    }*//*
                                    MessageUtils.showSnackBar(
                                        activity,
                                        snackviewForREg,
                                        registerRes?.message
                                    )
                                    MyUtils.passFragmentWithoutBackStack(activity,Login())
                                } else {
                                    MessageUtils.showSnackBar(
                                        activity,
                                        snackviewForREg,
                                        registerRes?.message
                                    )
                                }

                            } else {
                                Log.d("CANNOTASSOGN", "POJO")
                            }*/

                            val jsonobject = JSONObject(response.body()?.string())
                            val success = jsonobject.getBoolean("success")
                            val message = jsonobject.getString("message")
                            if (success) {
                                MessageUtils.showSnackBar(
                                    activity,
                                    snackviewForREg,
                                    message
                                )
                                MyUtils.passFragmentWithoutBackStack(activity, Login())
                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    snackviewForREg,
                                    message
                                )
                            }

                        } else {
                            val message = MessageUtils.showErrorCodeToast(response.code())
                            MessageUtils.showSnackBar(activity, snackviewForREg, message)
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val message = MessageUtils.showFailureToast(t.message)
                    MessageUtils.showSnackBar(activity, snackviewForREg, message)
                }

            })


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validateforregister(
        customername: String,
        mobile: String,
        email: String,
        password: String,
        organisation: String,
        department: String,
        designation: String
    ): Boolean {
        try {
            if (customername.isEmpty()) {
                MessageUtils.showSnackBar(activity, snackviewForREg, "Customer Name can't be empty")
                return false
            } else if (email.isEmpty() || !Pattern.matches(patternStr, email)) {
                MessageUtils.showSnackBar(activity, snackviewForREg, "Email ID can't be empty")
                return false
            } else if (mobile.isEmpty() || mobile.length != 10) {
                MessageUtils.showSnackBar(activity, snackviewForREg, "Mobile Number can't be empty")
                return false
            } else if (password.isEmpty()) {
                MessageUtils.showSnackBar(activity, snackviewForREg, "Password can't be empty")
                return false
            } else if (organisation.isEmpty()) {
                MessageUtils.showSnackBar(activity, snackviewForREg, "Organization can't be empty")
                return false
            } else if (department.isEmpty()) {
                MessageUtils.showSnackBar(activity, snackviewForREg, "Department can't be empty")
                return false
            } else if (designation.isEmpty()) {
                MessageUtils.showSnackBar(activity, snackviewForREg, "Designation can't be empty")
                return false
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return true
    }

    fun Window.getSoftInputMode(): Int {
        return attributes.softInputMode
    }
}
