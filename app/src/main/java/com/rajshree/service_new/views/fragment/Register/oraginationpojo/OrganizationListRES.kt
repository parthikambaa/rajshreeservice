package com.rajshree.service_new.views.fragment.Register.oraginationpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class OrganizationListRES(

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("organization")
	val organization: ArrayList<OrganizationItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)