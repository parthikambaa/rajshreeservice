@file:Suppress("UNREACHABLE_CODE")

package com.rajshree.service_new.views.fragment


import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.UpdateAvailability
import com.rajshree.service_new.R
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager.Companion.KEY_MOBILE
import com.rajshree.service_new.views.activity.CustomNavigation
import com.rajshree.service_new.views.fragment.enquiry.Enquiry
import com.rajshree.service_new.views.fragment.login.Login
import kotlinx.android.synthetic.main.fragment_splash_screen.*

class SplashScreen : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as CustomNavigation).disableToolAndSliderBar()
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
        //activity?.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        no_internet.visibility = View.GONE

        /*  Handler().postDelayed({
              try {
                  if (LocalSessionManager.isLogin(activity!!)) {
                      MyUtils.passFragmentWithoutBackStack(activity, Enquiry())
                  } else {
                      MyUtils.passFragmentWithoutBackStack(activity, Login())
                  }
  //                MyUtils.passFragmentWithoutBackStack(activity, Login())
              } catch (ex: Exception) {
                  ex.printStackTrace()
              }
          }, 2000)*/

        no_internet?.setOnClickListener {
            val intent = Intent()
            //$DataUsageSummaryActivity
            intent.component = ComponentName("com.android.settings", "com.android.settings.Settings")
            activity?.startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        if (MyUtils.isNetworkConnected(activity)) {
            no_internet.visibility = View.GONE
            checkUpdateViaPlayStore()
        } else {
            //MessageUtils.showSnackBarAction(activity, snack_view_splash, "No Internet Connection")
            no_internet.visibility = View.VISIBLE
        }
        //updateNew()
    }

    private fun updateNew() {

        val updateManager = AppUpdateManagerFactory.create(activity)
        updateManager.appUpdateInfo
            .addOnSuccessListener {
                Log.d("updates", "Play Store" + it.updateAvailability())
                if (it.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    Log.d("updates", "DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS")
                    updateManager.startUpdateFlowForResult(
                        it,
                        IMMEDIATE,
                        activity,
                        201
                    )
                } else if (it.updateAvailability() == UpdateAvailability.UPDATE_NOT_AVAILABLE) {
                    Log.d("updates", "UPDATE_NOT_AVAILABLE")
                    updateManager.startUpdateFlowForResult(
                        it,
                        AppUpdateType.FLEXIBLE,
                        activity,
                        201
                    )
                } else if (it.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                    Log.d("updates", "UPDATE_AVAILABLE")
                } else if (it.updateAvailability() == UpdateAvailability.UNKNOWN) {
                    Log.d("updates", "UNKNOWN")
                }
            }

    }


    private fun checkUpdateViaPlayStore() {
        // Creates instance of the manager.
        try {
            val appUpdateManager = AppUpdateManagerFactory.create(activity)

            // Returns an intent object that you use to check for an update.
            appUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
                Log.d(
                    "appUpdateManager",
                    "" + appUpdateInfo.packageName() + "  " + appUpdateInfo.availableVersionCode() + "  " + appUpdateInfo.installStatus() + " available " + appUpdateInfo.updateAvailability()
                )
                // Checks that the platform will allow the specified type of update.
                Log.d("appUpdateManager", "after_ " + UpdateAvailability.UPDATE_AVAILABLE)
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)
                ) {
                    Toast.makeText(activity, "Yes goto Update", Toast.LENGTH_SHORT).show()
                    // Request the update.

                    appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                        IMMEDIATE,
                        // The current activity making the update request.
                        activity,
                        // Include a request code to later monitor this update request.
                        201
                    )

                } else {
                    Log.d("appUpdateManager", "after_UPDATE_AVAILABLE " + UpdateAvailability.UPDATE_AVAILABLE)
                    Toast.makeText(activity, "No Update Found", Toast.LENGTH_SHORT).show()
                     Log.d("Username ",""+LocalSessionManager.getUserInfo(activity!!,KEY_MOBILE))
                    if (LocalSessionManager.isLogin(activity!!)) {
                        MyUtils.passFragmentWithoutBackStack(activity, Enquiry())
                    } else {
                        MyUtils.passFragmentWithoutBackStack(activity, Login())
                    }


                    /*appUpdateManager.startUpdateFlowForResult(
                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                    appUpdateInfo,
                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    activity,
                    // Include a request code to later monitor this update request.
                    201
                )*/

                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.printStackTrace(), Toast.LENGTH_SHORT).show()
        }
    }

}
