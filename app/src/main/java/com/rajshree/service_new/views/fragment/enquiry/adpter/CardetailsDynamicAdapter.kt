package com.rajshree.service_new.views.fragment.enquiry.adpter

import android.support.design.widget.TextInputEditText
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter

import android.widget.Spinner
import com.google.gson.Gson

import com.rajshree.service_new.R
import com.rajshree.service_new.Utils.MessageUtils
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager.Companion.KEY_TOKEN

import com.rajshree.service_new.views.fragment.enquiry.carDetails
import com.rajshree.service_new.views.fragment.enquiry.carmodelpojo.CarModelRES
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CardetailsDynamicAdapter(
    val context: FragmentActivity,
    val caradetaiArrayList: ArrayList<carDetails>,
    val carbrand: ArrayList<String>
) : RecyclerView.Adapter<CardetailsDynamicAdapter.ViewHolder>() {


    var carModelRES:CarModelRES?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_car_details,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return caradetaiArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            Log.d("ADAPTer",""+carbrand[position])
            var currentpostion =position
            holder.carbrandsp?.adapter =ArrayAdapter(context,android.R.layout.simple_dropdown_item_1line,android.R.id.text1,carbrand)

/*           *//* holder.carmodel?.setOnClickListener {
                try{
                    if(holder.carbrand.text.toString().isEmpty()){
                        MessageUtils.showToastMessage(context,"Car Brand is can't be empty")
                    }else{
                        getCarmodel(holder.carbrand.text.toString().trim())
                        Log.d("adapter",""+carmodel.size)

                        if(carmodel.size!=0){
                            holder.carmodel.setAdapter(ArrayAdapter<String>(context,android.R.layout.simple_dropdown_item_1line,android.R.id.text1,carmodel))
                            holder.carmodel.threshold=1
                        }else{
                        }
                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }*//*
            holder.carbrand.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    caradetaiArrayList[position].carBrand =s.toString()
                    try{
                        if(holder.carbrand.text.toString().isEmpty()){
                            MessageUtils.showToastMessage(context,"Car Brand is can't be empty")
                        }else{
                            getCarmodel(holder.carbrand.text.toString().trim(),holder)
                            Log.d("adapter",""+carmodel.size)
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    Log.d("before text changed",""+s?.toString())
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    Log.d("onchange text changed",""+s?.toString())
                }

            })
            holder.carmodel?.addTextChangedListener(object:TextWatcher{
                override fun afterTextChanged(s: Editable?) {
                    caradetaiArrayList[position].carmodel =s.toString()

                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    Log.d("before text changed",""+s?.toString())
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    Log.d("onchange text changed",""+s?.toString())

                }

            })*/


            holder.carbrandsp?.onItemSelectedListener =object:AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                    val selectedItem =holder.carbrandsp?.selectedItem.toString()
                    Log.d("addpter",""+selectedItem)
                    holder.carbrand?.setText(selectedItem)
                    if (selectedItem == context.getString(R.string.select_car_brand)){
                        MessageUtils.showToastMessage(context,context.getString(R.string.select_car_brand))
                    }else{
                      caradetaiArrayList[currentpostion].carBrand = selectedItem
                        getCarmodel(selectedItem,holder.carmodelsp)

                    }
                }

            }
            holder.carmodelsp?.onItemSelectedListener =object :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val selecteditem =holder.carmodelsp?.selectedItem.toString()

                    holder.carmodel?.setText(selecteditem)
                    caradetaiArrayList[currentpostion].carmodel =selecteditem
                }

            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun getCarmodel(
        carbrand: String,
        carmodelsp: Spinner?
    ):ArrayList<String> {
        val dialog =MessageUtils.showDialogWhite(context)
        val carmodel=ArrayList<String>()
        val carmodels=ArrayList<String>()
        val map =HashMap<String,Any>()
        map["carmake"]= carbrand
        val service = MyUtils.getRetroService()!!
        val callback = service.call_post("appgetcarmodel","Bearer "+LocalSessionManager.getUserInfo(context,KEY_TOKEN),map)
        callback.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if(response.isSuccessful){
                        MessageUtils.dismissDialog(dialog)
                        carModelRES =Gson().fromJson(response.body()?.string(),CarModelRES::class.java)
                        if(carModelRES!=null){
                                    if(carModelRES?.carmodel?.size!=0){
                                        carmodel.clear()
                                        for(item in carModelRES?.carmodel!!){
                                            Log.d("Adpter",""+item?.carmodel)
                                            carmodel.add(item?.carmodel!!)
                                        }
                                        carmodelsp?.adapter = ArrayAdapter<String>(context,android.R.layout.simple_dropdown_item_1line,android.R.id.text1,carmodel)
                                    }else{
                                    }

                        }else{
                        }
                    }else{
                    }
                } catch (eX: Exception) {
                    eX.printStackTrace()
                }
            }
        })

        return carmodel
    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
     /*   var carmodel = itemView?.findViewById<AutoCompleteTextView>(R.id.enq_customer_car_model_actv_adp)!!
        var carbrand = itemView?.findViewById<AutoCompleteTextView>(R.id.enq_customer_what_cars_actv_adp)!!  */
        val carmodel = itemView?.findViewById<TextInputEditText>(R.id.enq_customer_car_model_edt_adp)
        val carbrand = itemView?.findViewById<TextInputEditText>(R.id.enq_customer_what_cars_edt_adp)
        val carmodelsp = itemView?.findViewById<Spinner>(R.id.enq_customer_car_model_sp_adp)
        val carbrandsp = itemView?.findViewById<Spinner>(R.id.enq_customer_what_cars_sp_adp)
    }
}