package com.rajshree.service_new.views.fragment.enquiry.car_brandspojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CarBrandRES(

	@field:SerializedName("regsiter")
	val regsiter: List<RegsiterItem?>? = null,

	@field:SerializedName("carmake")
	val carmekes: List<CarmekesItem?>? = null,

	@field:SerializedName("success")
	val success: String? = null,

	@field:SerializedName("enquiry")
	val enquiry: List<EnquiryItem?>? = null,

	@field:SerializedName("city")
	val city: List<CityItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)