package com.rajshree.service_new.views.fragment.enquiry.car_brandspojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CarmekesItem(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("carmake")
	val carmake: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)