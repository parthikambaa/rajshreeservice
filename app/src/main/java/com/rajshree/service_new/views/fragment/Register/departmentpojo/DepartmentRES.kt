package com.rajshree.service_new.views.fragment.Register.departmentpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DepartmentRES(

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("departments")
	val departments: List<DepartmentsItem?>? = null,

	@field:SerializedName("designation")
	val designation: List<DesignationItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)