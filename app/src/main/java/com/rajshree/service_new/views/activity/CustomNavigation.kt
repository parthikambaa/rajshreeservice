package com.rajshree.service_new.views.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.infideap.drawerbehavior.Advance3DDrawerLayout
import com.rajshree.service_new.R
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.apiandlocalsession.ClickEventValues
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager.Companion.KEY_EMAIL
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager.Companion.KEY_NAME
import com.rajshree.service_new.views.activity.adapter.MenuListAdapter
import com.rajshree.service_new.views.activity.pojo.MenuListItem
import com.rajshree.service_new.views.fragment.SplashScreen
import com.rajshree.service_new.views.fragment.enquiry.Enquiry
import com.rajshree.service_new.views.fragment.listOfEnquiry.ListOfEnquiry
import com.rajshree.service_new.views.fragment.login.Login
import customviews.CustomTextView
import kotlinx.android.synthetic.main.activity_custom_navigation.*
import kotlinx.android.synthetic.main.header.*

class CustomNavigation : AppCompatActivity(), ClickEventValues {

    companion object {
        var snackView: View? = null
    }

    var drawerLayout: Advance3DDrawerLayout? = null
    var recyMenuList: RecyclerView? = null
    var mainContainer: FrameLayout? = null
    var imgbtnBackpress: ImageView? = null
    var menuListAdapter: MenuListAdapter? = null
    var menuArrayList: ArrayList<MenuListItem> = ArrayList()
    var toolbar: Toolbar? = null
    var toolTitle: CustomTextView? = null
    var doubleBackToExitPressedOnce: Boolean = false

    /*Logout Dialog Funciotnality*/
    var logoutdiabuil: AlertDialog.Builder? = null
    var logoutdialog: AlertDialog? = null
    var logout = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_navigation)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        supportActionBar?.hide()
        supportActionBar?.setDisplayShowTitleEnabled(false)
        logoutdiabuil = AlertDialog.Builder(this)
        snackView = findViewById(R.id.main_container)
        drawerLayout = findViewById(R.id.drawer_layout)
        mainContainer = findViewById(R.id.main_container)
        imgbtnBackpress = findViewById(R.id.ic_back_crm)
        toolbar = findViewById(R.id.toolbar)
        toolTitle = findViewById(R.id.tool_title)
        nav_user_email?.isSelected = true
        toolTitle?.text = getString(R.string.enquiry)

        /**
         * Set To Menu Navigation Menus
         */


        val myenquiries = MenuListItem(R.drawable.my_enquies, getString(R.string.my_enquries).toUpperCase())
        menuArrayList.add(myenquiries)

        val privacypolicy = MenuListItem(R.drawable.ic_privacy, getString(R.string.privacy_policy).toUpperCase())
        menuArrayList.add(privacypolicy)

        val logoutmenu = MenuListItem(R.drawable.logout, getString(R.string.logout).toUpperCase())
        menuArrayList.add(logoutmenu)

        /**
         * set to Adpter
         */
        recycle_menu_list?.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        menuListAdapter = MenuListAdapter(this, menuArrayList, this)
        recycle_menu_list?.adapter = menuListAdapter

        /**
         * Call SplashScreen
         */

        try {
            logout = intent.getBooleanExtra("logout", false)
            when (logout) {
                true -> {
                    MyUtils.passFragmentBackStack(this@CustomNavigation, Login())
                }
                else -> {
                    MyUtils.passFragmentBackStack(this@CustomNavigation, SplashScreen())
                }
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }


        //Read and set version code Automatically

        try {
            val versionName = packageManager.getPackageInfo(packageName, 0).versionName;
            version_code_txt?.text = "v$versionName"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace();
        }
        /**
         * setonclickListener
         */
        imgbtnBackpress?.setOnClickListener {
            if (drawerLayout?.isDrawerOpen(Gravity.START)!!) {
                drawerLayout?.closeDrawer(Gravity.START)
            } else {
                drawerLayout?.openDrawer(Gravity.START)
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val mesg = "resultCode $resultCode requestCode $requestCode"
        Log.d("appUpdateManager", "onActivityResult  $mesg")
        if (201 == requestCode) {
            if (resultCode == RESULT_OK) {
                Log.d("appUpdateManager", "onActivityResult  Update Success")
                Toast.makeText(this, "Update Success", Toast.LENGTH_SHORT).show()
                val loginIntent = Intent(applicationContext, CustomNavigation::class.java)
                loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(loginIntent)
                finish()
            } else {
                Log.d("appUpdateManager", "onActivityResult  Update failed")
                Toast.makeText(this, "Update failed", Toast.LENGTH_SHORT).show();
            }
        }
    }


    override fun getClickedItemValues(position: Int, name: String) {
        try {
            //Log.d("Onclicked position", "" + name)
            when {
                name.toLowerCase() == "LOG OUT".toLowerCase() -> {

                    logoutdialog()
                }
                name.toLowerCase() == "My Enquiries".toLowerCase() -> {
                    //MyUtils.passFragmentBackStackwithani(this,ListOfEnquiry())
                    drawerLayout?.closeDrawer(Gravity.START)
                    startActivity(Intent(this, ListOfEnquiry::class.java))
                    overridePendingTransition(0, 0)
                }
                else -> {
                    drawerLayout?.closeDrawer(Gravity.START)
                    MyUtils.PrivacyPolicy(this, MyUtils.PRIVACY_POLICY)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun homeFrgment(title: String) {
        Log.d("titlesdsdf", "" + title)

        toolbar?.visibility = View.VISIBLE
        toolTitle?.text = title
        drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        imgbtnBackpress?.setImageResource(R.drawable.menu)
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        imgbtnBackpress?.setOnClickListener {
            if (drawerLayout?.isDrawerOpen(Gravity.START)!!) {
                drawerLayout?.closeDrawer(Gravity.START)
            } else {
                drawerLayout?.openDrawer(Gravity.START)
            }
        }
        nav_cust_name?.text = LocalSessionManager.getUserInfo(this, KEY_NAME)
        nav_user_email?.text = LocalSessionManager.getUserInfo(this, KEY_EMAIL)
    }

    fun enableViewTitle(title: String) {

        toolTitle?.text = title
        toolbar?.visibility = View.VISIBLE
        drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        imgbtnBackpress?.setImageResource(R.drawable.ic_arrow_back_white)
        imgbtnBackpress?.setOnClickListener {
            onBackPressed()
        }
    }

    fun disableToolAndSliderBar() {
        try {
            drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            toolbar?.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            supportActionBar?.hide()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onBackPressed() {
        val ff = supportFragmentManager.findFragmentById(R.id.main_container)
        if (drawerLayout?.isDrawerOpen(GravityCompat.START)!!) {
            drawerLayout?.closeDrawer(GravityCompat.START)
        } else if (ff is Enquiry) {
            if (drawerLayout?.isDrawerOpen(Gravity.START)!!) {
                drawerLayout?.closeDrawer(Gravity.START)
            } else {
                checkBackPress()
            }
        } else if (ff is Login || ff is SplashScreen) {
            checkBackPress()
        } else {
            super.onBackPressed()
        }
    }

    public fun checkBackPress() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finish()
            return
        }

        doubleBackToExitPressedOnce = true

        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 1000)
    }

    public fun backpresss() {
        onBackPressed()

    }

    private fun logoutdialog() {
        try {
            val dialogLister = DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        //Yes button clicked
                        dialog.dismiss()
                        LocalSessionManager.logout(this)
                    }

                    DialogInterface.BUTTON_NEGATIVE ->
                        //No button clicked
                        dialog.dismiss()
                }

            }

            logoutdiabuil?.setCancelable(false)
            logoutdiabuil?.setMessage(getString(R.string.sure_to_logout))
            logoutdiabuil?.setPositiveButton("YES", dialogLister)
            logoutdiabuil?.setNegativeButton("NO", dialogLister)
            logoutdialog = logoutdiabuil?.create()
            logoutdialog?.show()


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        try {
            val view = currentFocus
            if (view != null && (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) && view is EditText && !view.javaClass.name.startsWith(
                    "android.webkit."
                )
            ) {
                val scrcoords = IntArray(2)
                view.getLocationOnScreen(scrcoords)
                val x = ev.rawX + view.left - scrcoords[0]
                val y = ev.rawY + view.top - scrcoords[1]
                if (x < view.left || x > view.right || y < view.top || y > view.bottom)
                    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                        this.window.decorView.applicationWindowToken,
                        0
                    )
            }
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

        return super.dispatchTouchEvent(ev)
    }

}
