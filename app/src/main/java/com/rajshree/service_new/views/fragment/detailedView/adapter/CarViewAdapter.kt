package com.rajshree.service_new.views.fragment.detailedView.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.rajshree.service_new.R
import com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo.EnquirydetailItem

class CarViewAdapter(
    val activity: FragmentActivity,
    val cardetaArrayList: ArrayList<EnquirydetailItem?>?
    ):RecyclerView.Adapter<CarViewAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.adapter_view_car_details,parent,false))
    }

    override fun getItemCount(): Int { return  cardetaArrayList?.size!!   }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    try{

        holder.carbrand?.text =cardetaArrayList!![position]?.carBrand
        holder.carmodel?.text =cardetaArrayList[position]?.carModel

    } catch (ex:Exception){
        ex.printStackTrace()
    }

    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        val carbrand  =itemView?.findViewById<TextView>(R.id.carmake_adp)
        val carmodel  =itemView?.findViewById<TextView>(R.id.carmodel_adp)

    }

}
