package com.rajshree.service_new.views.fragment.enquiry.car_brandspojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class EnquiryItem(

	@field:SerializedName("pincode")
	val pincode: Any? = null,

	@field:SerializedName("occupation")
	val occupation: String? = null,

	@field:SerializedName("city")
	val city: Any? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("created_at")
	val createdAt: Any? = null,

	@field:SerializedName("enquirydetail")
	val enquirydetail: List<Any?>? = null,

	@field:SerializedName("userid")
	val userid: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: Any? = null,

	@field:SerializedName("whatscars")
	val whatscars: String? = null,

	@field:SerializedName("howmanycars")
	val howmanycars: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("carmodel")
	val carmodel: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("carlookingcost")
	val carlookingcost: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)