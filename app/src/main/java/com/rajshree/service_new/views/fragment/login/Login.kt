package com.rajshree.service_new.views.fragment.login


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import com.google.gson.Gson
import com.rajshree.service_new.R
import com.rajshree.service_new.Utils.MessageUtils
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager
import com.rajshree.service_new.views.activity.CustomNavigation
import com.rajshree.service_new.views.activity.CustomNavigation.Companion.snackView
import com.rajshree.service_new.views.fragment.Register.Register
import com.rajshree.service_new.views.fragment.login.pojo.LoginRES
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login : Fragment() {
    private val patternStr = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    var dialog: Dialog? = null
    var loginres: LoginRES? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as CustomNavigation).disableToolAndSliderBar()
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        /**
         * set onclick listener
         */
        LocalSessionManager.LocalSessionManager(activity!!)
        loginbtn?.setOnClickListener {
            try {
                if (validateformat(
                        login_username?.text.toString(),
                        login_password?.text.toString()
                    )
                ) {
                    if (MyUtils.isNetworkConnected(activity)) {
                        callApi()
                    } else {
                        MessageUtils.showNetworkDialog(activity)
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        registerbtn?.setOnClickListener {
            try {
                MyUtils.passFragmentBackStack(activity, Register())
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

/*        forgotbtn?.setOnClickListener {
            try{

            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }*/
        /*   logintitle?.setOnClickListener {
               try{
                   MyUtils.passFragmentWithoutBackStack(activity, ListOfEnquiry())
               }catch (ex:Exception){
                   ex.printStackTrace()
               }
           }*/
        changepassword?.setOnClickListener {
            try {
                changepasswordpopup()

            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    private fun changepasswordpopup() {
        try {
            val changedialog = Dialog(activity)

            changedialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            changedialog.setContentView(R.layout.changepassword)
            changedialog.window.setBackgroundDrawableResource(R.drawable.rounded_diag)
            changedialog.window.setGravity(Gravity.BOTTOM)
            changedialog.setCancelable(false)
            changedialog.show()
            val snackview = changedialog.findViewById<ImageButton>(R.id.img_cancel)
            changedialog.findViewById<ImageButton>(R.id.img_cancel).setOnClickListener {
                try {
                    if (changedialog.isShowing) {
                        changedialog.dismiss()
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

            val submit: Button = changedialog.findViewById(R.id.submit)
            submit.setOnClickListener {
                try {
                    dialog = MessageUtils.showDialogWhite(activity)
                    val calinterface = MyUtils.getRetroService()
                    val map = HashMap<String, Any>()
                    map["mobile"] = changedialog.findViewById<EditText>(R.id.edt_mobile).text.toString()
                    map["newpassword"] = changedialog.findViewById<EditText>(R.id.edt_newpassword).text.toString()
                    Log.d("NewPasswordMAP", "" + map)
                    val callback = calinterface.call_post("resetpassword", map)
                    callback.enqueue(object : Callback<ResponseBody> {
                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            MessageUtils.dismissDialog(dialog)
                            val message = MessageUtils.showFailureToast(t.message)
                            MessageUtils.showSnackBar(activity, snackview, message)
                        }

                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            MessageUtils.dismissDialog(dialog)
                            try {
                                if (response.isSuccessful) {
                                    val obj = JSONObject(response.body()?.string())
                                    val success = obj.getBoolean("success")
                                    val message = obj.getString("message")
                                    if (success) {
                                        if (changedialog.isShowing) {
                                            changedialog.dismiss()
                                        }
                                        MessageUtils.showToastMessage(activity, message)
                                    } else {
                                        MessageUtils.showSnackBar(activity, snackview, message)

                                    }

                                } else {
                                    val message = MessageUtils.showErrorCodeToast(response.code())
                                    MessageUtils.showSnackBar(activity, snackview, message)
                                }
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }

                    })
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }


    }

    private fun callApi() = try {
        dialog = MessageUtils.showDialogWhite(activity)
        val calinterface = MyUtils.getRetroService()
        val map = HashMap<String, Any>()
        map["username"] = login_username?.text.toString()
        map["password"] = login_password?.text.toString()
//            val callback = calinterface.call_post("login", map)//Local Server
        val callback = calinterface.call_post("applogin", map)
        callback.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val message = MessageUtils.showFailureToast(t.message)
                MessageUtils.showSnackBar(activity, snackView, message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                MessageUtils.dismissDialog(dialog)
                try {
                    if (response.isSuccessful) {
                        loginres = Gson().fromJson(response.body()?.string(), LoginRES::class.java)
                        if (loginres != null) {
                            if (loginres?.success!!) {
                                if (loginres?.data != null) {
                                    LocalSessionManager.CreateSession(
                                        activity!!,
                                        loginres?.data?.userdetails?.name!!,
                                        loginres?.data?.userdetails?.mobile!!,
                                        loginres?.data?.userdetails?.departmentId!!,
                                        loginres?.data?.accessToken!!,
                                        loginres?.data?.userdetails?.companyId!!,
                                        loginres?.data?.userdetails?.email!!,
                                        loginres?.data?.userdetails?.id!!
                                    )
                                } else {
                                    MessageUtils.showSnackBar(activity, snackView, loginres?.message)
                                }
                            } else {
                                MessageUtils.showSnackBar(activity, snackView, loginres?.message)
                            }

                        } else {
                            Log.d("CANNOTASSOGN", "POJO")
                            val message = MessageUtils.showErrorCodeToast(response.code())
                            MessageUtils.showSnackBar(activity, snackView, message)
                        }
                    } else {
                        val message = MessageUtils.showErrorCodeToast(response.code())
                        MessageUtils.showSnackBar(activity, snackView, message)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        })
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    private fun validateformat(username: String, password: String): Boolean {
        try {
            when {
                username.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, snackView, "Mobile Number can't be empty")
                    return false
                }
                username.length != 10 -> {
                    MessageUtils.showSnackBar(activity, snackView, "Invalid Mobile Number")
                    return false
                }
                password.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, snackView, "Password can't be empty")
                    return false
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }

}
