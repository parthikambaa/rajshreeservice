package com.rajshree.service_new.views.fragment.enquiry.car_brandspojo

import com.google.gson.annotations.SerializedName

class CityItem(
       @SerializedName("id")
       var id:Int?=null,
       @SerializedName("city")
       var city:String?=null,
       @SerializedName("cr  eated_at")
       var createdAt:String?=null,
       @SerializedName("updated_at")
       var updatedAt:String?=null
)

