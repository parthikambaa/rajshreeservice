package com.rajshree.service_new.views.fragment.enquiry.carmodelpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CarmodelItem(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("carmodel")
	val carmodel: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("carmake")
	val carmake: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)