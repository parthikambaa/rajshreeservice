package com.rajshree.service_new.views.fragment.detailedView

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.rajshree.service_new.R
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.views.fragment.detailedView.adapter.CarViewAdapter
import com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo.EnquiriesItem
import kotlinx.android.synthetic.main.fragment_view_items_shows.*


class ViewItemsShows : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_view_items_shows)
        supportActionBar?.title = getString(R.string.enquiry_details)
        //supportActionBar?.setDisplayHomeAsUpEnabled(true)
/*
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        (activity as CustomNavigation).enableViewTitle("Enquiry Details")
        return inflater.inflate(R.layout.fragment_view_items_shows, container, false)
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)*/
        val enquiriesItem: EnquiriesItem = intent!!.extras.getSerializable("item") as EnquiriesItem
        name.text = MyUtils.nullValidation(enquiriesItem.name.toString().trim())
        email.text = MyUtils.nullValidation(enquiriesItem.email.toString().trim())
        mobile.text = MyUtils.nullValidation(enquiriesItem.mobile.toString().trim())
        pincode.text = MyUtils.nullValidation("" + enquiriesItem.pincode.toString().trim())
        view_looking_for.text = MyUtils.nullValidation(enquiriesItem.carlookingcost.toString().trim())

        txtVehicle.text =MyUtils.nullValidation(enquiriesItem.lookingVehicle.toString())
        txtInsuranceExpiryDate.text =MyUtils.nullValidation(enquiriesItem.vehicleExpiry.toString())


        val carViewAdapter = CarViewAdapter(this, enquiriesItem.enquirydetail)
        view_car_det_recyl?.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        view_car_det_recyl?.adapter = carViewAdapter
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        overridePendingTransition(0,0)

    }
}
