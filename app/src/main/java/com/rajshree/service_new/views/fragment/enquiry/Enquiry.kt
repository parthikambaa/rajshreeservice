package com.rajshree.service_new.views.fragment.enquiry

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.CalendarMode
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.rajshree.service_new.R
import com.rajshree.service_new.Utils.DateInputMask
import com.rajshree.service_new.Utils.DateUtils
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.Utils.MyUtils.MAHINDRA
import com.rajshree.service_new.Utils.MyUtils.SALES
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager.Companion.KEY_TOKEN
import com.rajshree.service_new.views.activity.CustomNavigation
import com.rajshree.service_new.views.activity.CustomNavigation.Companion.snackView
import com.rajshree.service_new.views.fragment.enquiry.adpter.CardetailsDynamicAdapter
import com.rajshree.service_new.views.fragment.enquiry.car_brandspojo.CarBrandRES
import com.rajshree.service_new.views.fragment.enquiry.carmodelpojo.CarModelRES
import com.rajshree.service_new.views.fragment.listOfEnquiry.ListOfEnquiry
import customviews.CustomRadioButton
import customviews.CustomTextView
import kotlinx.android.synthetic.main.fragment_enquiry.*
import kotlinx.android.synthetic.main.sales_mahindra_drop.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import com.rajshree.service_new.Utils.MessageUtils as MessageUtils


@Suppress("UNREACHABLE_CODE", "IMPLICIT_CAST_TO_ANY")
class Enquiry : Fragment() {

    val patternStr = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    var dialog: Dialog? = null
    var adpter: CardetailsDynamicAdapter? = null

    /*    var carModelSTRList=ArrayList<String>()
        var carMakeSTRList=ArrayList<String>()
        var citySTRList=ArrayList<String>()
        var  citySTRAdapter:ArrayAdapter<String>?=null
        var  carModelSTRAdapter:ArrayAdapter<String>?=null
        var  carMakeSTRAdapter:ArrayAdapter<String>?=null*/
    companion object {
        val cardetArrayList = ArrayList<carDetails>()
    }

    //var carBrandRES: CarBrandRES? = null
    var carbrand = ArrayList<String>()
    var carModelRES:CarModelRES?=null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //(activity as CustomNavigation).homeFrgment(getString(R.string.enquiry))
        return inflater.inflate(R.layout.fragment_enquiry, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity!!)

        if (MyUtils.isNetworkConnected(activity)) {
            getCarmodel(MAHINDRA,spSalesMahidra)
        } else {
            MessageUtils.showSnackBar(activity, snackView, getString(R.string.check_internet))
        }



        /**
         * set on Click Listener
         */
        enq_cancel_btn?.setOnClickListener {
            try {
                (activity as CustomNavigation).backpresss()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        /**
         * on text changer
         */
        enq_customer_how_many_car_edt?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null && !s.isEmpty()) {
                    val value = Integer.parseInt(StringBuilder(s!!).toString())

                    if (value <= 4) {
                        setTorecylerviewadapter(value)
                    } else {
                        MessageUtils.showSnackBar(activity, snackView, getString(R.string.crossing_limit))
                        enq_customer_how_many_car_edt?.setText("")
                    }
                } else {
                    cardetArrayList.clear()
                    if (adpter != null) {
                        adpter?.notifyDataSetChanged()
                    }
                }

            }

        })

        enq_add_btn?.setOnClickListener {
            try {
                val name = enq_customer_name_edt?.text.toString()
                val email = enq_email_id_edt?.text.toString()
                val mobile = enq_mob_no_edt?.text.toString()
                val howmanycars = enq_customer_how_many_car_edt?.text.toString()
                val lookingfor = view.findViewById<RadioButton>(rgSalesService.checkedRadioButtonId).text.toString()
                val lookingForvehicleName = if(lookingfor == SALES){ spSalesMahidra?.selectedItem.toString() }else{ enq_service_edt?.text.toString()}
                val pincode = enq_customer_pincode_edt?.text.toString()
                val expiry = edtInsuranceEndDate?.text.toString()
                val city = enq_customer_city_edt?.text.toString()
                val enq_occupation = enq_occupation?.text.toString()
                val map = HashMap<String, Any>()

                map["name"] = name
                map["email"] = email
                map["mobile"] = mobile
                map["howmanycars"] = howmanycars
                map["carlookingcost"] = lookingfor
                map["looking_vehicle"] = lookingForvehicleName
                map["vehicle_expiry"] = expiry
                map["pincode"] = pincode
                map["occupation"] = enq_occupation
                map["city"] = city
                if (validateForm(name, email, mobile, howmanycars, enq_occupation, city, pincode)) {
                    if (validatecardetails(cardetArrayList)) {
                        val hahsmap = HashMap<String, Any>()
                        hahsmap["car_detail"] = cardetArrayList
                        map["car_details"] = hahsmap
                        registeredServer(map)
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


        // Selected Sales or Service radio Group Listener
        rdbtnServices?.isChecked = true
        llySalesCarsDropDown?.visibility =View.GONE
        enq_service_edt?.visibility =View.VISIBLE
        
        rgSalesService?.setOnCheckedChangeListener { group, checkedId ->
            val selectedText = view.findViewById<CustomRadioButton>(checkedId).text.toString()
            Log.d("RadioGroupChanger",""+selectedText)
            if(selectedText == SALES){
                llySalesCarsDropDown?.visibility =View.VISIBLE
                enq_service_edt?.visibility =View.GONE
            } else{
                enq_service_edt?.visibility =View.VISIBLE
                llySalesCarsDropDown?.visibility =View.GONE

            }

        }

        //Mobile number Validation Only Allow 10 Digits
        enq_mob_no_edt?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s?.length!=10){
                    enq_mob_no_edt?.error = "Mobile number  10 digits"
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s?.isNotEmpty()!!){
                    val value=(StringBuilder(s!!).toString()).toLong()
                    Log.d("Changer","$s $value $count")
                    if(value<6){
                     enq_mob_no_edt.setText("6")
                        enq_mob_no_edt.setSelection(s.length)
                    }
                }

            }
        })

        // Pincode  Change Validation
        enq_customer_pincode_edt?.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s?.length!=6){
                    enq_customer_pincode_edt?.error = "Pincode  6 digits"
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s?.isNotEmpty()!!){
                    val value=Integer.parseInt(StringBuilder(s!!).toString())
                    Log.d("Changer","$s $value ")
                    if(value<6){
                        enq_customer_pincode_edt.setText("6")
                        enq_customer_pincode_edt.setSelection(s.length)
                    }
                }
            }

            
        })


        //DatepickerDialog
        edtInsuranceEndDate?.setOnClickListener {
            try{
//                showDatePickerDialog()
//                customCalendarDialog()

            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }

        edtInsuranceEndDate?.addTextChangedListener(DateInputMask(edtInsuranceEndDate))


        enq_customer_city_Sp.onItemSelectedListener =object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                try {
                    val  selectedItem =enq_customer_city_Sp?.selectedItem.toString()
                    enq_customer_city_edt.setText(selectedItem)
                    if (selectedItem == MyUtils.SELECT_CITY){

                    }else{

                    }

                } catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

        }

    }


    /**
     *  Custom Calender View
     */
    @SuppressLint("SetTextI18n")
    private fun customCalendarDialog() {
        try {
            val calenderDialog = Dialog(activity!!)
            Log.d(" DateMilli", "customCalendarDialog")
            calenderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            calenderDialog.setCancelable(false)
            calenderDialog.window?.setGravity(Gravity.CENTER)
            calenderDialog.window?.setBackgroundDrawable(activity!!.getDrawable(R.drawable.yellow_round))
            calenderDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
            calenderDialog.setContentView(R.layout.dialog_custom_calender)
            calenderDialog.show()
            var  calendar =Calendar.getInstance()
            /**
             * Default date picker to
             */
            val datepick = calenderDialog.findViewById<DatePicker>(R.id.datepicker)
            datepick.init(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH),
                DatePicker.OnDateChangedListener { view, year, monthOfYear, dayOfMonth ->
                Log.d("Year",""+year)
                Log.d("monthOfYear",""+monthOfYear)
                Log.d("dayOfMonth",""+dayOfMonth)

                 edtInsuranceEndDate.setText(DateUtils.convertDateFormate("$year-$monthOfYear-$dayOfMonth","yyyy-MM-dd","dd-MM-yyyy"))

            })
            datepick.firstDayOfWeek =Calendar.MONDAY
            calendar.add(Calendar.YEAR,-1)
            datepick.minDate = calendar.timeInMillis
            calendar.add(Calendar.YEAR,4)
            datepick.maxDate = calendar.timeInMillis




            calenderDialog.findViewById<CustomTextView>(R.id.txtExpiredCustomCalender).setOnClickListener {
                try {
                    calenderDialog.dismiss()
                    edtInsuranceEndDate.setText(getString(R.string.expried))
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

            calenderDialog.findViewById<ImageView>(R.id.imgCalenderCancel).setOnClickListener {
                try {
                    calenderDialog.dismiss()

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
           val  customCalendarView = calenderDialog.findViewById<MaterialCalendarView>(R.id.clnCustomCalender)
            /**
             * set Default Value For
             */

                customCalendarView.selectedDate = CalendarDay.today()
                customCalendarView.state().edit()
                    .setFirstDayOfWeek(Calendar.MONDAY)
                    .setMinimumDate(CalendarDay.from(calendar.get(Calendar.YEAR)-1,calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)) )
                    .setMaximumDate(CalendarDay.from(calendar.get(Calendar.YEAR)+3,calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)) )
                    .setCalendarDisplayMode(CalendarMode.MONTHS)
                    .commit()
                customCalendarView.selectedDate =CalendarDay.today()

            customCalendarView?.setOnDateChangedListener { widget, date, selected ->
                Log.d("Customcalnder",""+selected)
                Log.d("Customcalnder",""+date.date)


            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    

    override fun onResume() {
        super.onResume()
        Log.d("ENquory", "OnResume")
        (activity as CustomNavigation).homeFrgment(getString(R.string.enquiry))
    }

    private fun validatecardetails(cardetArrayList: ArrayList<carDetails>): Boolean {
        try {
            Log.d("cardetArrayList", "" + cardetArrayList.size)
            var check = true
            for (item in cardetArrayList) {
                if (item.carBrand.isNullOrEmpty() || item.carmodel.isNullOrEmpty()) {
                    check = false
                }
            }
            return if (check) {
                true
            } else {
                MessageUtils.showSnackBar(activity, snackView, "Car details is  can't be empty")
                false

            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }

    private fun setTorecylerviewadapter(value: Int) {
        try {
            cardetArrayList.clear()
            for (i: Int in 1..value) {
                val cardetails = carDetails("", "")
                cardetArrayList.add(cardetails)
            }
            adpter = CardetailsDynamicAdapter(activity!!, cardetArrayList, carbrand)
            recyler_enq_customer_cars?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recyler_enq_customer_cars?.adapter = adpter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validateForm(
        name: String,
        email: String,
        mobile: String,
        howmanycars: String,
        occupation: String,
        city: String,
        pincode: String
    ): Boolean {
        try {
            when {
                name.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, snackView, "Name can't be empty")
                    return false
                } /*else if (email.isEmpty()) {
                        MessageUtils.showSnackBar(activity, snackView, "Email ID can't be empty")
                        return true
                    } else if (!Pattern.matches(patternStr, email)) {
                        MessageUtils.showSnackBar(activity, snackView, "Invalid Email ID")
                        return false
                    }*/
                mobile.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, snackView, "Mobile can't be empty")
                    return false
                }
                mobile.length != 10 -> {
                    MessageUtils.showSnackBar(activity, snackView, "Invalid Mobile Number")
                    return false
                }

                MyUtils.SELECT_CITY==city  -> {
                    MessageUtils.showSnackBar(activity, snackView, "Select city")
                    return false
                }
                pincode.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, snackView, "PinCode can't be empty")
                    return false
                }
                occupation.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, snackView, "Occupation  can't be empty")
                    return false
                }
                howmanycars.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, snackView, "Car Qty can't be empty")
                    return false
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }


    private fun registeredServer(map: HashMap<String, Any>) {
        try {
            map["userid"] = LocalSessionManager.getUserInfo(activity!!, LocalSessionManager.KEY_ID)
            dialog = MessageUtils.showDialogWhite(activity)
            val calinterface = MyUtils.getRetroService()

//            val  callback = calinterface.call_post("addenquiry",map)//Local serve
            val callback = calinterface.call_post(
                "appaddenquiry",
                "Bearer " + LocalSessionManager.getUserInfo(activity!!, LocalSessionManager.KEY_TOKEN),
                map
            )
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val message = MessageUtils.showFailureToast(t.message)
                    MessageUtils.showSnackBar(activity, snackView, message)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            val jsonObject = JSONObject(response.body()?.string())
                            val success = jsonObject.getBoolean("success")
                            val message = jsonObject.getString("message")
                            if (success) {
                                MessageUtils.showToastMessage(activity, message)
                                //MyUtils.passFragmentBackStack(activity, ListOfEnquiry())
                                clearText()
                            } else {
                                MessageUtils.showSnackBar(activity, snackView, message)
                            }

                        } else {
                            val message = MessageUtils.showErrorCodeToast(response.code())
                            MessageUtils.showSnackBar(activity, snackView, message)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun clearText() {

        enq_customer_name_edt?.setText("")
        enq_email_id_edt?.setText("")
        enq_mob_no_edt?.setText("")
        enq_customer_how_many_car_edt?.setText("")
        enq_looking_for?.setText("")
        enq_customer_pincode_edt?.setText("")
        enq_occupation?.setText("")
        enq_customer_name_edt?.requestFocus()

        startActivity(Intent(activity, ListOfEnquiry::class.java))
    }


    fun Window.getSoftInputMode(): Int {
        return attributes.softInputMode
    }


    private fun getData() {
        dialog = MessageUtils.showDialogWhite(activity)
        val map = HashMap<String, Any>()
        val service = MyUtils.getRetroService()!!
        val callback =
            service.call_post("appgetdata", "Bearer " + LocalSessionManager.getUserInfo(activity!!, KEY_TOKEN), map)

        callback.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                //Log.d("failure0", "opposite")
                MessageUtils.dismissDialog(dialog)
                val message = MessageUtils.showFailureToast(t.message)
                MessageUtils.showSnackBar(activity, snackView, message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                //Log.d("failure0", "opposite_111")
                MessageUtils.dismissDialog(dialog)
                try {
                    val carBrandRES: CarBrandRES = Gson().fromJson(response.body()?.string(), CarBrandRES::class.java)
                    if (carBrandRES != null) {
                        // Log.d("carmodelslkdsldk", "" + carBrandRES?.carmekes?.size)
                        if (carBrandRES.carmekes?.size != 0) {

                            for (item in carBrandRES?.carmekes!!) {
                                //  Log.d("Cardata", "" + item?.carmake)
                                carbrand.add(item?.carmake!!)
                            }
                            carbrand.add(0, getString(R.string.select_car_brand))
                            //getCarModels

                        } else {
                            MessageUtils.showToastMessage(activity, carBrandRES.message)
                        }
                        if(carBrandRES.city?.size!=0){

                            val  cityList =ArrayList<String>()

                            carBrandRES.city?.forEachIndexed { index, cityItem ->
                               cityList.add(cityItem?.city!!)
                            }
                            cityList.add(0,MyUtils.SELECT_CITY)
                            if (cityList.size==1){
                             cityList.clear()
                              cityList.add("Data Not Available")
                            }
                            enq_customer_city_Sp.adapter = ArrayAdapter<String>(activity!!,android.R.layout.simple_dropdown_item_1line,cityList)

                        }

                    } else {
                        val message = MessageUtils.showErrorCodeToast(response.code())
                        MessageUtils.showToastMessage(activity, message)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        })
    }



    private fun getCarmodel(
        carbrand: String,
        carmodelsp: Spinner?
    ):ArrayList<String> {
        val dialog =MessageUtils.showDialogWhite(activity!!)
        val carmodel=ArrayList<String>()
        val carmodels=ArrayList<String>()
        val map =HashMap<String,Any>()
        map["carmake"]= carbrand
        val service = MyUtils.getRetroService()!!
        val callback = service.call_post("appgetcarmodel","Bearer "+LocalSessionManager.getUserInfo(activity!!,KEY_TOKEN),map)
        callback.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if(response.isSuccessful){
                        MessageUtils.dismissDialog(dialog)
                        carModelRES =Gson().fromJson(response.body()?.string(), CarModelRES::class.java)
                        if(carModelRES!=null){
                            if(carModelRES?.carmodel?.size!=0){
                                carmodel.clear()
                                for(item in carModelRES?.carmodel!!){
                                    Log.d("Adpter",""+item?.carmodel)
                                    carmodel.add(item?.carmodel!!)
                                }
                                carmodelsp?.adapter = ArrayAdapter<String>(activity!!,android.R.layout.simple_dropdown_item_1line,android.R.id.text1,carmodel)
                                getData()
                            }else{
                            }

                        }else{
                        }
                    }else{
                    }
                } catch (eX: Exception) {
                    eX.printStackTrace()
                }
            }
        })

        return carmodel
    }





}

class carDetails(
    @field:SerializedName("car_brand")
    var carBrand: String? = null,
    @field:SerializedName("car_model")
    var carmodel: String? = null
)