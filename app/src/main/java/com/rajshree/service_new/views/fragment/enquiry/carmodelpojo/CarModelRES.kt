package com.rajshree.service_new.views.fragment.enquiry.carmodelpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CarModelRES(

	@field:SerializedName("carmodel")
	val carmodel: ArrayList<CarmodelItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)