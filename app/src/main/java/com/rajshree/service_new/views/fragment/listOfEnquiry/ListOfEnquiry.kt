package com.rajshree.service_new.views.fragment.listOfEnquiry


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatSpinner
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import com.google.gson.Gson
import com.rajshree.service_new.R

import com.rajshree.service_new.Utils.MessageUtils
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager.Companion.KEY_ID
import com.rajshree.service_new.apiandlocalsession.LocalSessionManager.Companion.KEY_TOKEN
import com.rajshree.service_new.views.fragment.enquiry.Enquiry
import com.rajshree.service_new.views.fragment.listOfEnquiry.adapter.EnquiryListAdapter
import com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo.EnquiryListRES
import kotlinx.android.synthetic.main.fragment_list_of_enquiry.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class ListOfEnquiry : AppCompatActivity() {
    var TAG: String = "ValuesOfTesting"
    lateinit var activity: ListOfEnquiry
    var dialog: Dialog? = null
    var snackviewenqhome: View? = null
    var enquiryAdpter: EnquiryListAdapter? = null
    lateinit var enquiryRES: EnquiryListRES
    lateinit var filterSpnr: AppCompatSpinner
    val filterType = arrayOf("Today", "Last Week", "Last Month", "All")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //enquiryRES = EnquiryListRES()
        activity = this@ListOfEnquiry
        setContentView(R.layout.fragment_list_of_enquiry)
        supportActionBar?.title = getString(R.string.enquiry_details)
        snackviewenqhome = findViewById(R.id.home_enq_parent_view)
        //setSupportActionBar(my_toolbar)


        filter_img.isFocusable = true
        filter_img.isFocusableInTouchMode = true
        filter_img.requestFocus()
        filter_img.isEnabled = true

        filter_img.adapter = ArrayAdapter(activity, android.R.layout.simple_list_item_1, android.R.id.text1, filterType)

        //callAPI(1)

        filter_img.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                // Display the selected item text on text view
                Log.d(TAG, "Spinner selected : ${parent.getItemAtPosition(position).toString()}")
                checkOnFilter(parent.getItemAtPosition(position))

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }


        enq_lis_add_btn?.setOnClickListener {
            try {
                MyUtils.bottomtoTop(activity, Enquiry())
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        ic_back_crm?.setOnClickListener {
            onBackPressed()
        }
        recy_enq_list?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            @SuppressLint("RestrictedApi")
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    enq_lis_add_btn!!.visibility = View.GONE
                } else {
                    enq_lis_add_btn!!.visibility = View.GONE
                }
            }

        })
    }

    private fun checkOnFilter(itemAtPosition: Any?) {
        var onFoDays: Int = 0
        when (itemAtPosition) {
            filterType[0] -> onFoDays = 1
            filterType[1] -> onFoDays = 7
            filterType[2] -> {
                val calender = Calendar.getInstance()
                val last_month = calender.getActualMaximum(Calendar.DAY_OF_MONTH)
                onFoDays = last_month
            }
            filterType[3] -> onFoDays = 0
        }

        callAPI(onFoDays)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_, menu)
        filterSpnr = menu.findItem(R.id.action_filter).actionView as AppCompatSpinner
        val dilterType = arrayOf("Today", "Last Week", "Laset Month", "All")
        filterSpnr.adapter = ArrayAdapter(activity, android.R.layout.simple_list_item_1, android.R.id.text1, dilterType)

        filterSpnr.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                // Display the selected item text on text view
                Log.d(TAG, "Spinner selectedsdfs : ${parent.getItemAtPosition(position).toString()}")
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }
        /*val action_all = menu.findItem(R.id.action_all).actionView as AppCompatTextView
        val action_one_week = menu.findItem(R.id.action_one_week).actionView as AppCompatTextView
        val action_one_month = menu.findItem(R.id.action_one_month).actionView as AppCompatTextView
        val action_default = menu.findItem(R.id.action_default).actionView as AppCompatTextView*/


        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        val id = item?.getItemId()
        Log.d(TAG, "" + id)
        filterSpnr.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                // Display the selected item text on text view
                Log.d(TAG, "Spinner selected : ${parent.getItemAtPosition(position).toString()}")
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }
        return super.onOptionsItemSelected(item)
    }
/*
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when {
            item?.itemId == R.id.action_default -> {

                setToAdapter(enquiryRES, 1)
                //callAPI(1)
            }
            item?.itemId == R.id.action_one_week -> {

                setToAdapter(enquiryRES, 7)
                //callAPI(7)
            }
            item?.itemId == R.id.action_one_month -> {

                val calender = Calendar.getInstance()
                val last_month = calender.getActualMaximum(Calendar.DAY_OF_MONTH)

                setToAdapter(enquiryRES, last_month) // Show on Last 30 days
                //callAPI(last_month)
            }
            item?.itemId == R.id.action_all -> {

                setToAdapter(enquiryRES, 0) //Show all Days

                //callAPI(0)

            }

        }

        return super.onOptionsItemSelected(item)


    }*/

    private fun callAPI(noOfDyas: Int) {
        if (MyUtils.isNetworkConnected(activity)) {
            getEnquiryList(noOfDyas)
        } else {
            enq_list_empty_view?.visibility = View.VISIBLE
            enq_list_empty_view?.text = getString(R.string.check_internet)
            MessageUtils.showSnackBar(activity, snackviewenqhome, getString(R.string.check_internet))
        }
    }

    private fun getEnquiryList(onOfDays: Int) {
        try {
            dialog = MessageUtils.showDialogWhite(activity)
            val map = HashMap<String, Any>()
            map["userid"] = LocalSessionManager.getUserInfo(activity!!, KEY_ID)
            map["noOfDays"] = onOfDays
            val callinterface = MyUtils.getRetroService()

            val callback = callinterface?.call_post(
                "applistofenquiry",
                "Bearer " + LocalSessionManager.getUserInfo(activity!!, KEY_TOKEN),
                map
            )

            dialog?.setOnKeyListener(DialogInterface.OnKeyListener { dialogInterface, i, keyEvent ->
                dialog?.dismiss()
                callback?.cancel()
                false
            })
            callback?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val message = MessageUtils.showFailureToast(t.message)
                    MessageUtils.showSnackBar(activity, snackviewenqhome, message)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            enquiryRES = Gson().fromJson(response.body()?.string(), EnquiryListRES::class.java)
                            if (enquiryRES != null) {
                                if (enquiryRES.success!!) {
                                    if (enquiryRES.data != null) {
                                        if (enquiryRES.data!!.enquiries?.size != 0) {
                                            recy_enq_list?.visibility = View.VISIBLE
                                            enq_list_empty_view?.visibility = View.GONE
                                            setToAdapter(enquiryRES, 1)
                                        } else {
                                            recy_enq_list?.visibility = View.GONE
                                            enq_list_empty_view?.visibility = View.VISIBLE
                                            enq_list_empty_view?.text = getString(R.string.no_data_found)

                                        }
                                    } else {
                                        enq_list_empty_view?.visibility = View.VISIBLE
                                        enq_list_empty_view?.text = enquiryRES.message
                                        recy_enq_list?.visibility = View.GONE

                                    }
                                } else {
                                    enq_list_empty_view?.visibility = View.VISIBLE
                                    enq_list_empty_view?.text = enquiryRES.message
                                    recy_enq_list?.visibility = View.GONE

                                }

                            } else {
                                recy_enq_list?.visibility = View.GONE
                                enq_list_empty_view?.visibility = View.VISIBLE
                                enq_list_empty_view?.text = getString(R.string.no_data_found)
                            }

                        } else {

                            val message = MessageUtils.showErrorCodeToast(response.code())
                            enq_list_empty_view?.visibility = View.VISIBLE
                            enq_list_empty_view?.text = message
                            recy_enq_list?.visibility = View.GONE
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToAdapter(enquiryRES: EnquiryListRES, noOfDays: Int) {
        Log.d(TAG, "" + noOfDays)
        try {
            if (enquiryRES != null) {
                if (enquiryRES.data != null) {
                    var count = 0
                    if (enquiryRES.data.enquiries != null) {
                        count = enquiryRES.data.enquiries.size
                    }

                    if (count != 0) {

                        /* val dayDifference: Long = getDayDifference(noOfDays)

                         var temp: ArrayList<EnquiriesItem?>? =
                             enquiryRES.data.enquiries?.clone() as ArrayList<EnquiriesItem?>

                         if (noOfDays != 0) {
                             enquiryRES.data.enquiries.forEachIndexed { _, enquiriesItem ->
                                 if (dayDifference < convertDateToMilles(enquiriesItem?.enquiry_date)) {
                                 } else {
                                     temp?.remove(enquiriesItem)
                                 }
                             }
                         }
                          try {
                            temp =MyUtils.sort( temp)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                         */


                        val tempCount = enquiryRES.data.enquiries?.size
                        if (tempCount != 0) {

                            Toast.makeText(activity, "$tempCount Enquires", Toast.LENGTH_SHORT).show()
                            recy_enq_list?.visibility = View.VISIBLE
                            enq_list_empty_view?.visibility = View.GONE
                            recy_enq_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                            enquiryAdpter = EnquiryListAdapter(activity, MyUtils.sort(enquiryRES.data.enquiries))
                            recy_enq_list?.adapter = enquiryAdpter

                        } else {
                            recy_enq_list?.visibility = View.GONE
                            enq_list_empty_view?.visibility = View.VISIBLE
                            enq_list_empty_view?.text = getString(R.string.no_data_found)
                        }

                    } else {

                        recy_enq_list?.visibility = View.GONE
                        enq_list_empty_view?.visibility = View.VISIBLE
                        enq_list_empty_view?.text = getString(R.string.no_data_found)
                    }
                } else {

                    recy_enq_list?.visibility = View.GONE
                    enq_list_empty_view?.visibility = View.VISIBLE
                    enq_list_empty_view?.text = getString(R.string.no_data_found)
                }
            } else {

                recy_enq_list?.visibility = View.GONE
                enq_list_empty_view?.visibility = View.VISIBLE
                enq_list_empty_view?.text = getString(R.string.no_data_found)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    /**
     * Get Current Date and set calendar. After minus user sorting Interval Days
     */

   /* private fun getDayDifference(noOfDays: Int): Long {
        val sDate = DateUtils.formatTime(System.currentTimeMillis(), DateUtils.YYYY_MM_DD)
        val dateFormat = SimpleDateFormat(DateUtils.YYYY_MM_DD, Locale.getDefault())
        val date: Date = dateFormat.parse(sDate)
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.DATE, -noOfDays)
        val yesterdayAsString = dateFormat.format(calendar.time)
        return convertDateToMilles(yesterdayAsString)
    }*/


    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(0, 0)
    }

}
