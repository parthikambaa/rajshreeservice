package com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class EnquiriesItem(

    @field:SerializedName("pincode")
    var pincode: Any? = null,

    @field:SerializedName("occupation")
    var occupation: String? = null,

    @field:SerializedName("city")
    var city: Any? = null,

    @field:SerializedName("mobile")
    var mobile: String? = null,

    @field:SerializedName("created_at")
    var createdAt: String? = null,

    @field:SerializedName("enquirydetail")
    var enquirydetail: ArrayList<EnquirydetailItem?>? = null,

    @field:SerializedName("userid")
    var userid: Int? = null,

    @field:SerializedName("updated_at")
    var updatedAt: String? = null,

    @field:SerializedName("whatscars")
    var whatscars: String? = null,

    @field:SerializedName("howmanycars")
    var howmanycars: String? = null,

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("carmodel")
    var carmodel: Any? = null,

    @field:SerializedName("id")
    var id: Int? = null,

    @field:SerializedName("carlookingcost")
    var carlookingcost: String? = null,
    @field:SerializedName("looking_vehicle")
    var lookingVehicle: String? = null,
    @field:SerializedName("vehicle_expiry")
    var vehicleExpiry: String? = null,

    @field:SerializedName("email")
    var email: String? = null,
    @field:SerializedName("enquiry_date")
    var enquiry_date: String? = null
) : Serializable