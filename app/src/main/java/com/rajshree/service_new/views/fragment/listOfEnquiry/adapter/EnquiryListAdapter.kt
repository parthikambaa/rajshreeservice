package com.rajshree.service_new.views.fragment.listOfEnquiry.adapter

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rajshree.service_new.R
import com.rajshree.service_new.views.fragment.detailedView.ViewItemsShows
import com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo.EnquiriesItem
import customviews.CustomTextView


class EnquiryListAdapter(
    val context: FragmentActivity,
    private val enquiryList: ArrayList<EnquiriesItem?>?
) : RecyclerView.Adapter<EnquiryListAdapter.ViewHoldr>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHoldr {
        return ViewHoldr(LayoutInflater.from(context).inflate(R.layout.adapter_enq_item, p0, false))
    }

    override fun getItemCount(): Int {
        return enquiryList?.size!!
    }

    override fun onBindViewHolder(p0: ViewHoldr, p1: Int) {
        try {
            var car_model = ""
            enquiryList!![p1]?.enquirydetail!!.forEachIndexed { index, enquirydetailItem ->

                if (index == enquiryList[p1]?.enquirydetail!!.size - 1) {
                    car_model += enquirydetailItem?.carBrand!!
                } else {
                    car_model += enquirydetailItem?.carBrand!! + ",\b"
                }
            }


            p0.carqty.text = enquiryList[p1]?.howmanycars
            p0.carmodel.text = car_model


            p0.item_view.setOnClickListener {

                /*val fragment: Fragment = ViewItemsShows()
                val bundle = Bundle()
                bundle.putSerializable("item", enquiryList[p1])
                fragment.arguments = bundle
                MyUtils.passFragmentBackStackwithani(context, fragment)*/


                val intent = Intent(this.context, ViewItemsShows::class.java)
                val bundle = Bundle()
                bundle.putSerializable("item", enquiryList[p1])
                intent.putExtras(bundle)
                context.startActivity(intent)
                context.overridePendingTransition(0, 0)


            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class ViewHoldr(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val carmodel = itemView.findViewById<CustomTextView>(R.id.enq_carmodel_adp)!!
        val carqty = itemView.findViewById<CustomTextView>(R.id.enq_car_qty_adp)!!
        val item_view = itemView.findViewById<CardView>(R.id.item_view)!!

    }
}


