package com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("enquiries")
	val enquiries: ArrayList<EnquiriesItem?>? = null
)