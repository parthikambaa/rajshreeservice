package com.rajshree.service_new.views.activity.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.rajshree.service_new.R
import com.rajshree.service_new.views.activity.pojo.MenuListItem
import com.rajshree.service_new.apiandlocalsession.ClickEventValues

class MenuListAdapter (val context: Context, val menuArrayList: ArrayList<MenuListItem>, val onclickEvnt: ClickEventValues)
    : RecyclerView.Adapter<MenuListAdapter.ViewHolderr>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MenuListAdapter.ViewHolderr {
        val view = LayoutInflater.from(this.context).inflate(R.layout.adapter_menu_list_item,p0,false)
        return  ViewHolderr(view)
    }

    override fun getItemCount(): Int { return this.menuArrayList.size}

    override fun onBindViewHolder(p0: MenuListAdapter.ViewHolderr, p1: Int) {
        try{
            p0.imageName.setImageResource(menuArrayList[p1].image!!)
            p0.name.setText(menuArrayList.get(p1).name)
            p0.linear.setOnClickListener {
                onclickEvnt.getClickedItemValues(p1,menuArrayList.get(p1).name!!)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolderr(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name : TextView = itemView.findViewById(R.id.menu_item_name)
        val imageName : ImageView = itemView.findViewById(R.id.menu_item_image)
        val linear : LinearLayout = itemView.findViewById(R.id.menuitem_adap)
    }
}