package com.rajshree.service_new.views.fragment.listOfEnquiry.enquirylistpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class EnquirydetailItem(

	@field:SerializedName("car_model")
	val carModel: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("car_brand")
	val carBrand: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("enquiry_id")
	val enquiryId: Int? = null
) : Serializable