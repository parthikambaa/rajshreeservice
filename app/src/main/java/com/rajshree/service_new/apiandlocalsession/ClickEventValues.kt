package com.rajshree.service_new.apiandlocalsession

interface ClickEventValues {
    fun getClickedItemValues(position: Int, name: String)
}