package com.rajshree.service_new.apiandlocalsession

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v4.app.FragmentActivity
import com.rajshree.service_new.views.activity.CustomNavigation
import com.rajshree.service_new.Utils.MessageUtils
import com.rajshree.service_new.Utils.MyUtils
import com.rajshree.service_new.views.fragment.enquiry.Enquiry



@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@SuppressLint("CommitPrefEdits")
class LocalSessionManager {
    companion object {
        var KEY_NAME = "name"
        var KEY_MOBILE = "mobile"
        var KEY_LOGIN_STATUS = "false"
        var DEPARTMENT = "user_type"
        var KEY_TOKEN = "token"
        var KEY_EMAIL = "email"
        var ORGANISATION="organoisation"
        var DESIGNATION="designation"
        var KEY_ID = "id"


        fun init(activity: Context): SharedPreferences {
            val pref = activity.getSharedPreferences("Rajshree", 0) // 0 - for private mode
            val editor = pref.edit()
            return pref
        }

        fun LocalSessionManager(activity: FragmentActivity) {
            val sharedPreferences =
                init(activity)
            val editor = sharedPreferences.edit()
        }

        fun CreateSession(
            activity: FragmentActivity,
            name: String,
            mobile: String,
            department: String,
            token: String,
            organisation: String,
            email: String,
            id : Int
        ) {
            val sharedPreferences =
                init(activity)
            val editor = sharedPreferences.edit()
            editor.putBoolean(KEY_LOGIN_STATUS, true)
            editor.putString(KEY_NAME, name)
            editor.putString(KEY_MOBILE, mobile)

            editor.putString(DEPARTMENT, department)
            editor.putString(KEY_TOKEN, token)

            editor.putString(ORGANISATION, organisation)

            editor.putString(KEY_EMAIL, email)
            editor.putString(KEY_ID, "" + id)


            editor.apply()

            MyUtils.passFragmentWithoutBackStack(activity, Enquiry())

        }

        fun getUserInfo(activity: Context, name: String): String {
            val pref = init(activity)
            //Log.d("szsass", "" + pref.getAll());
            return pref.getString(name, "")
        }

        fun logout(activity: FragmentActivity) {

            val pref = init(activity)
            val editor = pref.edit()
            editor.clear()
            editor.apply()
//        setHeaderText(activity)
            //listSetter.setList(setNavigationView(activity));
            MessageUtils.showToastMessage(activity, "Logout Success")

            activity.startActivity(Intent(activity, CustomNavigation::class.java).putExtra("logout",true))
            activity.overridePendingTransition(0, 0)

            activity.finish()
            //MyUtils.passFragmentWithoutBackStack(activity, new Login());

        }

        fun isLogin(activity: FragmentActivity): Boolean {
            val sharedPreferences =
                init(activity)
            return sharedPreferences.getBoolean(KEY_LOGIN_STATUS, false)
        }
    }

}