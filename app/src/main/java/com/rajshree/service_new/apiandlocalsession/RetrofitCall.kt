package com.rajshree.service_new.apiandlocalsession

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitCall {

    companion object {
        private var retrofit: Retrofit? = null
        //        var BASE_URL = "http://192.168.2.23:8009/api/auth/"//Local Api
//        var BASE_URL = "http://192.168.2.23:8007/api/auth/"//Local Api
//        var BASE_URL = "http://192.168.2.83:8007/api/auth/"//Local Api
//
        var BASE_URL = "http://35.154.245.147:8004/api/auth/"// demo Live Api
        val client: Retrofit?
            get() {
                val gson = GsonBuilder()
                    .setLenient()
                    .create()
                if (retrofit == null) {
                    retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(buildClient())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
                }
                return retrofit
            }


        private fun buildClient(): OkHttpClient {
            return OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
        }
    }

}
